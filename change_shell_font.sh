#!/usr/bin/env bash

cat ~/.bashrc > ~/.bashrc_backup_change_shell_font

cat <<'EOF' >> ~/.bashrc
choose_color(){
    echo -n "$(case $((RANDOM%2)) in 0) echo -n "3";; 1) echo -n "9";; esac)$((RANDOM%8))"
}
export PS1="$PS1 \[\e[\$(choose_color)m\]"
EOF
