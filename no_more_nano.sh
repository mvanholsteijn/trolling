#!/usr/bin/env bash

cat ~/.bashrc > ~/.bashrc_backup_no_more_nano

cat <<'EOF' >> ~/.bashrc
alias nano='echo "No more nano for you, time to learn vim!" && read && vim'
EOF
